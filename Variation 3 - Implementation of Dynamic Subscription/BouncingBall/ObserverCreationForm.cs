﻿using System;
using System.Windows.Forms;

namespace BouncingBall
{
    public partial class ObserverCreationForm : Form
    {
        public ObserverCreationForm()
        {
            InitializeComponent();
        }

        public string ObserverTitle
        {
            get { return titleTextBox.Text; }
            set { titleTextBox.Text = value; }
        }

        private void creaetionButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        public string ObserverType => (listTypeRadioButton.Checked) ? "L" : "G";
    }
}
