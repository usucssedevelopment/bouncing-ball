﻿using System;
using System.Drawing;

namespace BouncingBall
{
    public partial class GraphicalDisplay : BallObserver
    {
        public GraphicalDisplay()
        {
            InitializeComponent();
        }

        protected override void RefreshDisplay()
        {
            if (IsDisposed) return;

            using (var graphics = boxPanel.CreateGraphics())
            {
                graphics.Clear(Color.White);

                foreach (var ball in BallsBeingObserved)
                {
                    var pen = new Pen(Color.Black);
                    Brush brush = new SolidBrush(ball.Color);

                    var x = Convert.ToSingle(ball.X - ball.Radius);
                    var y = Convert.ToSingle(ball.Y - ball.Radius);
                    var width = Convert.ToSingle(ball.Radius * 2);
                    var height = Convert.ToSingle(ball.Radius * 2);

                    graphics.FillEllipse(brush, x, y, width, height);
                    graphics.DrawEllipse(pen, x, y, width, height);
                }
            }
        }

        private void GraphicalDisplay_Load(object sender, EventArgs e)
        {
            Text = Title;
            StartRefreshTimer();
        }
    }
}
