﻿using System;
using System.Windows.Forms;

namespace BouncingBall
{
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Box.Width = 600;
            Box.Height = 400;
            Box.Label = "My Box";

            Application.Run(new ControlForm());
        }
    }
}
