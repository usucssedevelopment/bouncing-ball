# Bouncing Balls

This repository contains variations of a bouncing ball system to illustrate the AME principles,
 as well as the Observer, Decorator and a Factory Patterns:

* Variation 1 - Basic Box with Balls bouncing around in it.
  * Creates a fix set of balls that move on their own within the box.
  * The balls are active objects
  
* Variation 2 - GUI with a simple Observer
  *  Creates fixed set of balls and two observers, each watching the full set of balls
  
* Variation 3 - User features for ball and observer creation, subscribing, and unsubscribing
  * The user can create observers and balls on demand.
  * The user can subscribe or unsubscribe balls with observers.
  * Improved unsubscribing so untracked balls are not displayed
  * Improved ListDisplay, with the addition of the Id column and better refresh performance

* Variation 4 - Decorations on Balls

* Variation 5 - Additional Shapes and Simply Factor

* Variation 6 - Layers and Factory Method

* Variation 7 - More Unit Testing
