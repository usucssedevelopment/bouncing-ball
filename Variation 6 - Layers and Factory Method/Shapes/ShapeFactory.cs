﻿namespace Shapes
{
    public class ShapeFactory
    {
        public virtual Shape Create(Specification specs)
        {
            if (string.IsNullOrWhiteSpace(specs?.ShapeType)) return null;

            Shape shape = null;

            switch (specs.ShapeType.Trim().ToLower())
            {
                case "ball":
                    shape = new Ball();
                    break;
                case "square":
                    shape = new Square();
                    break;
                case "triangle":
                    shape = new Triangle();
                    break;
            }
            if (shape != null)
                shape.MyBox = BoxContainingCreatedShapes;

            return shape;
        }
        public Box BoxContainingCreatedShapes { get; set; }

        public class Specification
        {
            public string ShapeType;
        }
    }
}
