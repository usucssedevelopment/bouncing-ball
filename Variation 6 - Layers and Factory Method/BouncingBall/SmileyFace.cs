﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shapes;

namespace BouncingBall
{
    public class SmileyFace : Ball
    {
        public override void Draw(Graphics graphics)
        {
            base.Draw(graphics);

            var smileRadius = Radius*0.80F;
            var x = X - smileRadius;
            var y = MyBox.Height - Y - smileRadius;
            var width = smileRadius * 2;
            var height = smileRadius * 2;

            graphics.DrawArc(LinePen, x, y, width, height, 40F, 100F);
        }
    }
}
