﻿using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace BouncingBall
{
    public class Subject
    {
        private readonly object _myLock = new object();

        public List<ShapeObserver> Subscribers { get; } = new List<ShapeObserver>();

        public void Subscribe(ShapeObserver observer)
        {
            lock (_myLock)
            {
                if (observer != null && !Subscribers.Contains(observer))
                    Subscribers.Add(observer);
            }
        }

        public void Unsubscribe(ShapeObserver observer)
        {
            lock (_myLock)
            {
                if (!Subscribers.Contains(observer)) return;

                observer.Remove(this);
                Subscribers.Remove(observer);
            }
        }

        public void Notify()
        {
            lock (_myLock)
            {
                foreach (var observer in Subscribers)
                    observer.Update(Clone());
            }
        }

        public virtual Subject Clone()
        {
            return MemberwiseClone() as Subject;
        }
    }
}
