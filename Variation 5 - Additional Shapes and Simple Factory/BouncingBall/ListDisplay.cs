﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;

namespace BouncingBall
{
    public partial class ListDisplay : ShapeObserver
    {
        public ListDisplay()
        {
            InitializeComponent();
        }

        protected override void RefreshDisplay()
        {
            SuspendLayout();
            RemoveUntrackedBalls();
            foreach (var shape in ShapesBeingObserved)
            {
                var index = FindIndex(shape);
                if (index == -1)
                    shapeListView.Items.Add(CreateListViewItem(shape));
                else
                    UpdateListViewItem(shape, shapeListView.Items[index]);
            }
            ResumeLayout();
        }

        private void ListDisplay_Load(object sender, EventArgs e)
        {
            Text = Title;
            StartRefreshTimer();
        }

        private void RemoveUntrackedBalls()
        {
            var removeList = new List<int>();
            for (var i = shapeListView.Items.Count - 1; i >= 0; i--)
            {
                if (IsShapeBeingObserved((int)shapeListView.Items[i].Tag)) continue;

                removeList.Add(i);
            }

            foreach (var index in removeList)
                shapeListView.Items.RemoveAt(index);
        }

        private int FindIndex(Shape ball)
        {
            var index = -1;

            for (var i = 0; i < shapeListView.Items.Count && index == -1; i++)
                if (ball.Id == (int)shapeListView.Items[i].Tag)
                    index = i;

            return index;
        }

        private static ListViewItem CreateListViewItem(Shape shape)
        {
            var item = new ListViewItem() { Tag = shape.Id };
            var values = SetupColumnValues(shape, item);
            foreach (var columnValue in values)
                item.SubItems.Add(columnValue);
            return item;
        }

        private static void UpdateListViewItem(Shape shape, ListViewItem item)
        {
            var values = SetupColumnValues(shape, item);
            for (var i = 0; i < values.Length; i++)
                item.SubItems[i] = values[i];
        }

        private static ListViewItem.ListViewSubItem[] SetupColumnValues(Shape shape, ListViewItem item)
        {
            var values = new ListViewItem.ListViewSubItem[7];
            values[0] = new ListViewItem.ListViewSubItem(item, shape.Id.ToString());
            values[1] = new ListViewItem.ListViewSubItem(item, shape.X.ToString("F1"));
            values[2] = new ListViewItem.ListViewSubItem(item, shape.Y.ToString("F1"));
            values[3] = new ListViewItem.ListViewSubItem(item, shape.Size.ToString(CultureInfo.InvariantCulture));
            values[4] = new ListViewItem.ListViewSubItem(item, shape.Direction.ToString(CultureInfo.InvariantCulture));
            values[5] = new ListViewItem.ListViewSubItem(item, shape.Speed.ToString(CultureInfo.InvariantCulture));
            values[6] = new ListViewItem.ListViewSubItem(item, shape.StateChanges.ToString());
            return values;
        }

    }
}
