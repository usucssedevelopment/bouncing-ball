﻿using System.Drawing;

namespace BouncingBall
{
    public class ShapeSpecification
    {
        public enum ShapeType { Ball, Square, Triangle }
        public ShapeType MyType { get; set; }

        public float InitialX { get; set; }
        public float InitialY { get; set; }
        public float InitialDirection { get; set; }
        public float InitialSpeed { get; set; }
        public float InitialSize { get; set; }
        public Color InitialColor { get; set; } = Color.Transparent;

        public bool HasChangableDirection { get; set; }
        public bool HasChangableSize { get; set; }
        public bool HasChangableSpeed { get; set; }
        public bool HasChangableColor { get; set; }

        public int DeltaSize { get; set; } = 10;
        public int DeltaSpeed { get; set; } = 6;
        public int DeltaDegrees { get; set; } = 60;
        public Color AltColor { get; set; } = Shape.RandomColor;
    }
}
