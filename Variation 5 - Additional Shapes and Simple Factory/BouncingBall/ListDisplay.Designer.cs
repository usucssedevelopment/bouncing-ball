﻿namespace BouncingBall
{
    partial class ListDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.shapeListView = new System.Windows.Forms.ListView();
            this.idColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.xColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.yColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.sizeColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.directionColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.speedColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.stateChangesColumnHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // shapeListView
            // 
            this.shapeListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.idColumnHeader,
            this.xColumnHeader,
            this.yColumnHeader,
            this.sizeColumnHeader,
            this.directionColumnHeader,
            this.speedColumnHeader,
            this.stateChangesColumnHeader});
            this.shapeListView.Location = new System.Drawing.Point(24, 26);
            this.shapeListView.Name = "shapeListView";
            this.shapeListView.Size = new System.Drawing.Size(522, 341);
            this.shapeListView.TabIndex = 0;
            this.shapeListView.UseCompatibleStateImageBehavior = false;
            this.shapeListView.View = System.Windows.Forms.View.Details;
            // 
            // idColumnHeader
            // 
            this.idColumnHeader.Text = "Id";
            this.idColumnHeader.Width = 52;
            // 
            // xColumnHeader
            // 
            this.xColumnHeader.Text = "X";
            this.xColumnHeader.Width = 80;
            // 
            // yColumnHeader
            // 
            this.yColumnHeader.Text = "Y";
            this.yColumnHeader.Width = 69;
            // 
            // sizeColumnHeader
            // 
            this.sizeColumnHeader.Text = "Size";
            // 
            // directionColumnHeader
            // 
            this.directionColumnHeader.Text = "Direction";
            this.directionColumnHeader.Width = 81;
            // 
            // speedColumnHeader
            // 
            this.speedColumnHeader.Text = "Speed";
            // 
            // stateChangesColumnHeader
            // 
            this.stateChangesColumnHeader.Text = "State Changes";
            this.stateChangesColumnHeader.Width = 86;
            // 
            // ListDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(570, 391);
            this.Controls.Add(this.shapeListView);
            this.Name = "ListDisplay";
            this.Text = "BallListDisplay";
            this.Load += new System.EventHandler(this.ListDisplay_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView shapeListView;
        private System.Windows.Forms.ColumnHeader xColumnHeader;
        private System.Windows.Forms.ColumnHeader yColumnHeader;
        private System.Windows.Forms.ColumnHeader sizeColumnHeader;
        private System.Windows.Forms.ColumnHeader directionColumnHeader;
        private System.Windows.Forms.ColumnHeader speedColumnHeader;
        private System.Windows.Forms.ColumnHeader stateChangesColumnHeader;
        private System.Windows.Forms.ColumnHeader idColumnHeader;
    }
}