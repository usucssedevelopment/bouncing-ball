﻿using System;
using System.Windows.Forms;

namespace BouncingBall
{
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var myBox = new Box()
            {
                Width = 600,
                Height = 400,
                Label = "My Box"
            };

            Application.Run(new ControlForm() { MyBox = myBox } );
        }
    }
}
