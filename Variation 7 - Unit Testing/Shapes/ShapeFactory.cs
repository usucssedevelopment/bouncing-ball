﻿using System.Drawing;

namespace Shapes
{
    public class ShapeFactory
    {
        public virtual Shape Create(Specification specs)
        {
            Shape shape = null;
            switch (specs.MyType)
            {
                case Specification.ShapeType.Ball:
                    shape = new Ball();
                    break;
                case Specification.ShapeType.Square:
                    shape = new Square();
                    break;
                case Specification.ShapeType.Triangle:
                    shape = new Triangle();
                    break;
            }

            if (shape == null) return null;

            shape.MyBox = BoxContainingCreatedShapes;
            shape.X = specs.X;
            shape.Y = specs.Y;
            shape.Direction = specs.Direction;
            shape.Speed = specs.Speed;
            shape.Size = specs.Size;
            shape.Color = specs.Color;
            shape.SetupDefaults();

            return shape;
        }
        public Box BoxContainingCreatedShapes { get; set; }

        public class Specification
        {
            public enum ShapeType { Ball, Square, Triangle }
            public ShapeType MyType { get; set; }

            public virtual float X { get; set; }
            public virtual float Y { get; set; }
            public virtual float Direction { get; set; }
            public virtual float Speed { get; set; }
            public virtual float Size { get; set; }
            public virtual Color Color { get; set; } = Color.Transparent;
        }
    }
}
