﻿using System;
using System.Windows.Forms;
using Shapes;

namespace BouncingBall
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var myBox = new Box()
            {
                Width = 600,
                Height = 400,
                Label = "My Box"
            };

            var myFactory = new CustomShapeFactory() {BoxContainingCreatedShapes = myBox};

            Application.Run(new ControlForm() { MyBox = myBox, MyFactory = myFactory });
        }
    }
}
